﻿/*
 * object to contain all items accessable to all control functions
 */
var globals = {};

// Tic Tac Toe application controller function
var TicTacToeController = function () {

	// Global variables canvas and context
	var canvas;
	var context;

	// Each section height and width
	var sectionHeight;
	var sectionWidth;

	var startGame = false;

	/*
	* Initialize Tic Tac Toe Application
	*/
	this.Initialize = function () {

		// Get animation frame, canvas and context
		canvas = document.getElementById("TicTacToeCanvas");
		context = canvas.getContext("2d");
		context.translate(0.5, 0.5);

		// Resize canvas to draw
		restartGame();

		// Add event 
		addEvent();
	}

	/*
	* Add events
	*/
	var addEvent = function () {

		// Add resize event to draw canvas everytime browser resize
		window.addEventListener('resize', resizeCanvas, false);

		// Add mouse up event to draw on mouse click
		canvas.addEventListener('mouseup', function (event) {

			if (startGame == false) {
				alert("Please click on start game button to proceed.");
				return;
			}

			// Get mouse click position
			var clickPosition = getClickPosition(event);

			// Get index position on game board and cordinate
			var indexAndPostion = findIndexAndPosition(clickPosition);

			// Check if it's players turn and not already occupied then fill it and move to computer's turn
			if (globals.game.status === "running" && globals.game.currentState.turn === "X" &&
				globals.game.currentState.board[indexAndPostion.index] === "E") {

				// Get new game state with move and make computer's move
				var next = new State(globals.game.currentState);
				next.board[indexAndPostion.index] = "X";

				// Draw Cross on user click position
				drawCross(indexAndPostion.xCordinate, indexAndPostion.yCordinate,
							indexAndPostion.xCordinate + sectionWidth, indexAndPostion.yCordinate + sectionHeight);

				// Make computer's next turn "O"
				next.nextTurn();

				globals.game.advanceTo(next);
			}
		});

		// Start game
		$('#StartGame').on('click', function () {

			// Set game status started
			startGame = true;
			initializeGameCoponents();

			// Update UI
			$('#StartGame').css('display', 'none');
			$('#Quit').css('display', 'inline');
		});

		// Quit game
		$('#Quit').on('click', function () {

			// Update UI
			$('#Quit').css('display', 'none');
			$('#StartGame').css('display', 'inline');

			// Set game status not started
			startGame = false;
			restartGame();
		});

		// Play Game Again
		$('#PlayAgain').on('click', function () {

			// Update UI
			$('#PlayAgain').css('display', 'none');
			$('#Result').fadeOut(1000);
			$('#Result').text();
			$('#Quit').css('display', 'inline');

			// Set game status not started
			startGame = true;

			// Restart game and initialize game components
			restartGame();
			initializeGameCoponents();
		});
	}

	/*
	* Restart game by setting board again
	*/
	var restartGame = function () {
		canvas.width = window.innerWidth * 0.6 < 400 ? 400: window.innerWidth * 0.6;
		canvas.height = window.innerHeight * 0.9 < 400 ? 400: window.innerHeight * 0.9;
		sectionHeight = canvas.height / 3;
		sectionWidth = canvas.width / 3;
		drawBoard();
	}

	/*
	* Initialize game components
	*/
	var initializeGameCoponents = function () {
		// Initialize bot player
		var aiPlayer = new AI();

		// Initialize game object
		globals.game = new Game(aiPlayer);

		// Set game object for player
		aiPlayer.plays(globals.game);

		// Start game
		globals.game.start();
	}

	/*
	* Resize canvas according to screen size and draw application
	*/
	var resizeCanvas = function () {
		canvas.width = window.innerWidth * 0.6 < 400 ? 400 : window.innerWidth * 0.6;
		canvas.height = window.innerHeight * 0.9 < 400 ? 400 : window.innerHeight * 0.9;
		sectionHeight = canvas.height / 3;
		sectionWidth = canvas.width / 3;
		drawBoard();

		// Maintain game state if game is already running
		if (startGame == true) {
			var boardState = globals.game.currentState.board;

			for (var index = 0; index < boardState.length; index++) {
				var x = Math.floor(index / 3);
				var y = index - (x * 3);
				var xCordinate = y * sectionWidth;
				var yCordinate = x * sectionHeight;

				if (boardState[index] === "X") {
					drawCross(xCordinate, yCordinate,
							xCordinate + sectionWidth, yCordinate + sectionHeight);
				}
				else if (boardState[index] === "O") {
					drawCircle(xCordinate, yCordinate);
				}
			}
		}
	}

	/*
	* Draw Tic Tac Toe application board
	*/
	var drawBoard = function () {
		// Take canvas height and width in temporary variable
		var canvasHeight = canvas.height;
		var canvasWidth = canvas.width;

		// Calculate step distance
		var xStepDistance = canvasWidth / 3;
		var yStepDistance = canvasHeight / 3;

		// Define line points for 3 X 3 canvas
		var linePositions = [
			{ startPosition: { x: xStepDistance, y: 0 }, endPosition: { x: xStepDistance, y: canvasHeight } },
			{ startPosition: { x: xStepDistance * 2, y: 0 }, endPosition: { x: xStepDistance * 2, y: canvasHeight } },
			{ startPosition: { x: 0, y: yStepDistance }, endPosition: { x: canvasWidth, y: yStepDistance } },
			{ startPosition: { x: 0, y: yStepDistance * 2 }, endPosition: { x: canvasWidth, y: yStepDistance * 2 } }];

		// Draw each line
		for (var i = 0; i < linePositions.length; i++) {
			drawLine(linePositions[i]);
		}
	}

	/*
	* Draw Line in canvas according to given points
	* @param linePosition : line position cordinates
	*/
	var drawLine = function (linePosition) {
		context.beginPath();
		context.lineWidth = 2;
		context.moveTo(linePosition.startPosition.x, linePosition.startPosition.y);
		context.lineTo(linePosition.endPosition.x, linePosition.endPosition.y);
		context.strokeStyle = "white";
		context.stroke();
	}

	/*
	* Get mouse click position on canvas
	* @param event : Mouse click event
	*/
	var getClickPosition = function (event) {
		var rectangle = canvas.getBoundingClientRect();
		return { x: event.clientX - rectangle.left, y: event.clientY - rectangle.top };
	}

	/*
	* Find index and cordinate position to draw piece
	* @param clickPosition [Fractional Number]: Mouse Click Position
	*/
	var findIndexAndPosition = function (clickPosition) {

		// Starting Cordinate position to draw piece 
		var xCordinate;
		var yCordinate;

		// Object that indicates index and starting cordinate position of mouse click
		var indexAndCordinate = {
			index: 0,
			xCordinate: 0,
			yCordinate: 0
		};

		for (var x = 0; x < 3; x++) {
			for (var y = 0; y < 3; y++) {
				xCordinate = y * sectionWidth;
				yCordinate = x * sectionHeight;

				// If cordinate position found then return
				if (clickPosition.x >= xCordinate && clickPosition.x <= xCordinate + sectionWidth &&
					clickPosition.y >= yCordinate && clickPosition.y <= yCordinate + sectionHeight) {
					indexAndCordinate.xCordinate = xCordinate;
					indexAndCordinate.yCordinate = yCordinate;

					return indexAndCordinate;
				}
				indexAndCordinate.index++;
			}
		}
		return indexAndCordinate;
	}

	/*
	* Draw circle on specified cordinate position
	* @param xCordinate [Fractional Number]: starting x-axis cordinate to draw
	* @param yCordinate [Fractional Number]: starting y-axis cordinate to draw
	*/
	var drawCircle = function (xCordinate, yCordinate) {

		// Find center point for circle
		var centerX = xCordinate + (0.5 * sectionWidth);
		var centerY = yCordinate + (0.5 * sectionHeight);

		// Find radius and angles for circle
		var radius = (sectionWidth - 100) / 2;
		var startAngle = 0 * Math.PI;
		var endAngle = 2 * Math.PI;

		// Draw circle on canvas
		context.lineWidth = 10;
		context.strokeStyle = "#01bBC2";
		context.beginPath();
		context.arc(centerX, centerY, radius, startAngle, endAngle);
		context.stroke();
	}

	/*
	* Draw Cross on specified cordinate position
	* @param startXCordinate [Fractional Number]: starting x-axis cordinate to draw
	* @param startYCordinate [Fractional Number]: starting y-axis cordinate to draw
	* @param endXCordinate [Fractional Number]: ending x-axis cordinate to draw
	* @param endYCordinate [Fractional Number]: ending y-axis cordinate to draw
	*/
	var drawCross = function (startXCordinate, startYCordinate, endXCordinate, endYCordinate) {
		var offset = 50;

		// Draw cross
		context.lineWidth = 10;
		context.strokeStyle = "#f1be32";
		context.beginPath();
		context.moveTo(startXCordinate + offset, startYCordinate + offset);
		context.lineTo(endXCordinate - offset, endYCordinate - offset);
		context.moveTo(endXCordinate - offset, startYCordinate + offset);
		context.lineTo(startXCordinate + offset, endYCordinate - offset);
		context.stroke();
	}

	/*
	* Display Computers turn
	* @param index [number]: position on board to draw piece
	*/
	this.drawOponentsTurn = function (index) {
		var x = Math.floor(index / 3);
		var y = index - (x * 3);
		var xCordinate = y * sectionWidth;
		var yCordinate = x * sectionHeight;
		drawCircle(xCordinate, yCordinate);
	}

	/*
	* On game over display result
	* @param pos [message]: message to be displayed
	*/
	this.gameOver = function (message) {
		$('#Quit').css('display', 'none');
		$('#PlayAgain').css('display', 'inline');
		$('#Result').text(message);
		$('#Result').fadeIn(1000);
	}
}

// Tic tac toe application
var tictactoe = new TicTacToeController();

// On document ready initialize application
$(document).ready(function () {
	tictactoe.Initialize();
});