﻿/// <reference path="tictactoe.js" />

/*
 * Constructs a game object to be played
 * @param autoPlayer [AIPlayer] : the AI player
 */
var Game = function (autoPlayer) {

	// Initialize the ai player for this game
	this.ai = autoPlayer;

	// Initialize game current state to new board configuration
	this.currentState = new State();

	// "E" stands for empty board cell
	this.currentState.board = ["E", "E", "E",
                               "E", "E", "E",
                               "E", "E", "E"];

	// X plays first
	this.currentState.turn = "X";


	// Initialize game status to beginning
	this.status = "beginning";

	/*
     * public function that advances the game to a new state
     * @param _state [State]: the new state to advance the game to
     */
	this.advanceTo = function (_state) {
		this.currentState = _state;
		if (_state.isTerminal()) {
			this.status = "ended";

			if (_state.result === "X-won") {
				tictactoe.gameOver("Player Won");
			}
			else if (_state.result === "O-won") {
				tictactoe.gameOver("Bot Won");
			}
			else {
				tictactoe.gameOver("It's Draw");
			}
		}
		else if (this.currentState.turn === "O") {
			// the game is still running
			// notify the AI player its turn has come up
			this.ai.notify("O");
		}
	};

	/*
     * Starts the game
     */
	this.start = function () {
		if (this.status = "beginning") {
			// Invoke advanceTo with the initial state
			this.advanceTo(this.currentState);
			this.status = "running";
		}
	}
};

/*
 * public static function that calculates the score of the x player in a given terminal state
 * @param _state [State]: the state in which the score is calculated
 * @return [Number]: the score calculated for the human player
 */
Game.score = function (_state) {
	if (_state.result === "X-won") {
		// the x player won
		return 10 - _state.oMovesCount;
	}
	else if (_state.result === "O-won") {
		//the x player lost
		return -10 + _state.oMovesCount;
	}
	else {
		//it's a draw
		return 0;
	}
}