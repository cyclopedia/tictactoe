﻿/*
 * Represents a state in the game
 * @param old [State]: old state to intialize the new state
 */
var State = function (old) {

	// The player who has the turn to play
	this.turn = "";

	// Number of moves of the bot player
	this.oMovesCount = 0;

	// Result of the game in this State
	this.result = "still running";

	// Board configuration in this state
	this.board = [];

	// Begin Object Construction 
	if (typeof old !== "undefined") {
		// if the state is constructed using a copy of another state
		var len = old.board.length;
		this.board = new Array(len);
		for (var itr = 0 ; itr < len ; itr++) {
			this.board[itr] = old.board[itr];
		}

		this.oMovesCount = old.oMovesCount;
		this.result = old.result;
		this.turn = old.turn;
	}

	// Next turn in a the state
	this.nextTurn = function () {
		this.turn = this.turn === "X" ? "O" : "X";
	}

	/*
     * public function that enumerates the empty cells in state
     * @return [Array]: Indexes of all empty cells
     */
	this.emptyCells = function () {
		var emptyIndexes = [];
		for (var index = 0; index < 9 ; index++) {
			if (this.board[index] === "E") {
				emptyIndexes.push(index);
			}
		}
		return emptyIndexes;
	}

	/*
     * public  function that checks if the state is a terminal state or not
     * the state result is updated to reflect the result of the game
     * @returns [Boolean]: true if it's terminal, false otherwise
     */
	this.isTerminal = function () {
		var currentBoard = this.board;

		// Check all rows
		for (var i = 0; i <= 6; i = i + 3) {
			if (currentBoard[i] !== "E" && currentBoard[i] === currentBoard[i + 1] && currentBoard[i + 1] == currentBoard[i + 2]) {
				// Update the state result
				this.result = currentBoard[i] + "-won";
				return true;
			}
		}

		// Check all columns
		for (var i = 0; i <= 2 ; i++) {
			if (currentBoard[i] !== "E" && currentBoard[i] === currentBoard[i + 3] && currentBoard[i + 3] === currentBoard[i + 6]) {
				// Update the state result
				this.result = currentBoard[i] + "-won";
				return true;
			}
		}

		// Check both diagonals
		for (var i = 0, j = 4; i <= 2 ; i = i + 2, j = j - 2) {
			if (currentBoard[i] !== "E" && currentBoard[i] == currentBoard[i + j] && currentBoard[i + j] === currentBoard[i + 2 * j]) {
				// Update the state result
				this.result = currentBoard[i] + "-won";
				return true;
			}
		}

		var available = this.emptyCells();
		if (available.length == 0) {
			//the game is draw
			this.result = "draw";
			return true;
		}
		return false;
	};
};