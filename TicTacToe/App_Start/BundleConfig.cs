﻿using System.Web.Optimization;

namespace TicTacToe.App_Start
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
					  "~/Scripts/bootstrap.js"));

			bundles.Add(new ScriptBundle("~/bundles/tictactoe").Include(
					  "~/Scripts/Site/tictactoe.js",
					  "~/Scripts/Site/gameState.js",
					  "~/Scripts/Site/game.js",
					  "~/Scripts/Site/botPlayer.js"));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/bootstrap.css",
					  "~/Content/bootstrap-theme.css",
					  "~/Content/site.css"));

			BundleTable.EnableOptimizations = true;
		}
	}
}